FROM debian:stable-20210816-slim as scratch
ENV DEBIAN_NONINTERACTIVE=noninteractive

FROM scratch as build

USER root
WORKDIR /build

ARG PROTOC_VERSION=3.15.8
ARG PROTOC_GEN_GO_VERSION=1.26
ARG PROTOC_GEN_GO_GRPC_VERSION=1.1
ARG GO_VERSION=1.16

ENV PROTOC_VERSION=$PROTOC_VERSION
ENV PROTOC_GEN_GO_VERSION=$PROTOC_GEN_GO_VERSION
ENV PROTOC_GEN_GO_GRPC_VERSION=$PROTOC_GEN_GO_GRPC_VERSION
ENV GO_VERSION=$GO_VERSION

# Prepare system
RUN apt-get update
RUN apt-get install -y curl unzip

# Install protoc
ENV PROTOC_ZIP=protoc-${PROTOC_VERSION}-linux-x86_64.zip
ENV PROTOC_URL=https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/${PROTOC_ZIP}
RUN curl -OL ${PROTOC_URL}
RUN unzip -o ${PROTOC_ZIP} -d /protoc
ENV PATH="$PATH:/protoc/bin"

# Install go
RUN curl -OL https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz
RUN mkdir /golang
RUN tar -C /golang -xzf go${GO_VERSION}.linux-amd64.tar.gz
ENV GOPATH="/golang/path"
ENV PATH="$PATH:/golang/go/bin:$GOPATH/bin"

# Install go protoc-gen
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v${PROTOC_GEN_GO_VERSION}
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v${PROTOC_GEN_GO_GRPC_VERSION}

FROM scratch as prod
COPY --from=build /protoc /protoc
COPY --from=build /golang /golang
ENV GOPATH="/golang/path"
ENV PATH="$PATH:/protoc/bin:/golang/go/bin:$GOPATH/bin"
RUN apt-get update
RUN apt-get install -y git